
function initialize(box)
	dofile(box:get_config("${Path_Data}") .. "/plugins/stimulation/lua-stimulator-stim-codes.lua")
	
	-- Global variables
	alea_max = 300 --msec
	answer_wait = 1.200 --sec
	
	xp_duration = box:get_setting(2) --sec
	target_display = box:get_setting(3)/1000 --sec 
	target_nbr = box:get_setting(4)
	distractor_nbr = box:get_setting(5)
	stim_nbr = target_nbr + distractor_nbr
	target_stim = _G[box:get_setting(6)]
	baseline_duration = box:get_setting(7) --sec
	if target_stim == OVTK_StimulationId_Number_00 then
		--box:log("Info", "Circle targets")
		target_nrm = OVTK_StimulationId_Number_01
		target_odd = OVTK_StimulationId_Number_02
		distractor_nrm = OVTK_StimulationId_Number_11
		distractor_odd = OVTK_StimulationId_Number_12
	else
		--box:log("Info", "Square targets")
		target_nrm = OVTK_StimulationId_Number_11
		target_odd = OVTK_StimulationId_Number_12
		distractor_nrm = OVTK_StimulationId_Number_01
		distractor_odd = OVTK_StimulationId_Number_02		
	end
	
	trials_duration = xp_duration / stim_nbr --sec
	
	wait = (answer_wait + alea_max/1000) --sec
	
	-- initializes random seed
	math.randomseed(os.time())
	-- fill the sequence table with predifined order
	sequence = {}
	
	for i = 1, target_nbr/4 do
		table.insert(sequence, 1, target_odd)
		table.insert(sequence, 1, target_nrm)
	end
	for i = 1, distractor_nbr/4 do
		table.insert(sequence, 1, distractor_odd)
		table.insert(sequence, 1, distractor_nrm)
	end
	
	-- randomize the sequence
	for i = 1, stim_nbr/4 do
		a = math.random(1, stim_nbr/2)
		b = math.random(1, stim_nbr/2)
		swap = sequence[a]
		sequence[a] = sequence[b]
		sequence[b] = swap
	end
	
	for i = 1, target_nbr/4 do
		table.insert(sequence, 1, target_odd)
		table.insert(sequence, 1, target_nrm)
	end
	for i = 1, distractor_nbr/4 do
		table.insert(sequence, 1, distractor_odd)
		table.insert(sequence, 1, distractor_nrm)
	end
	
	-- randomize the sequence
	for i = stim_nbr/4, stim_nbr/2 do
		a = math.random(1, stim_nbr/2)
		b = math.random(1, stim_nbr/2)
		swap = sequence[a]
		sequence[a] = sequence[b]
		sequence[b] = swap
	end
	
end

function process(box)

	local t=0

	-- manages baseline

	box:send_stimulation(1, OVTK_StimulationId_ExperimentStart, t, 0)
	t = t + 5

	box:send_stimulation(1, OVTK_StimulationId_BaselineStart, t, 0)
	box:send_stimulation(1, OVTK_StimulationId_Beep, t, 0)
	t = t + baseline_duration

	box:send_stimulation(1, OVTK_StimulationId_BaselineStop, t, 0)
	box:send_stimulation(1, OVTK_StimulationId_Beep, t, 0)
	t = t + 5

	-- manages trials
	for i = 1, stim_nbr do
		
		-- start of trial
		box:send_stimulation(1, OVTK_GDF_Start_Of_Trial, t, 0)
		
		-- random time before stimulation
		alea = math.random(0, alea_max)/1000
		t = t + alea
		
		-- display stimulation
		box:send_stimulation(1, sequence[i], t, 0)
		t = t + target_display
		box:send_stimulation(1, OVTK_StimulationId_VisualStimulationStop, t, 0)
		
		-- wait for the end of trial
		t = t + (trials_duration - alea - target_display)
		
		-- ends trial 
		box:send_stimulation(1, OVTK_GDF_End_Of_Trial, t, 0)
	end
	
	-- send end for completeness
	box:send_stimulation(1, OVTK_GDF_End_Of_Session, t, 0)
	t = t + 5

	box:send_stimulation(1, OVTK_StimulationId_Train, t, 0)
	t = t + 1
	
	-- used to cause the acquisition scenario to stop
	box:send_stimulation(1, OVTK_StimulationId_ExperimentStop, t, 0)

end
