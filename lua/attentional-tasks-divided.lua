
function initialize(box)
	dofile(box:get_config("${Path_Data}") .. "/plugins/stimulation/lua-stimulator-stim-codes.lua")
	
	-- Global variables
	answer_wait = 1.200 --sec
	
	xp_duration = box:get_setting(2) --sec 
	target_display = box:get_setting(3)/1000 --sec 
	stim_nbr = box:get_setting(4)
	trials_duration = xp_duration / stim_nbr --sec
	baseline_duration = box:get_setting(5) --sec
	
	-- fill the sequence table with predifined order
	sequence = {}
	for i = 1, stim_nbr/8 do
		--visual stimulations
		table.insert(sequence, 1, OVTK_StimulationId_Number_11)
		table.insert(sequence, 1, OVTK_StimulationId_Number_12)
		table.insert(sequence, 1, OVTK_StimulationId_Number_01)
		table.insert(sequence, 1, OVTK_StimulationId_Number_02)
		--auditory stimulations
		table.insert(sequence, 1, OVTK_StimulationId_Label_0A)
		table.insert(sequence, 1, OVTK_StimulationId_Label_0B)
		table.insert(sequence, 1, OVTK_StimulationId_Label_1A)
		table.insert(sequence, 1, OVTK_StimulationId_Label_1B)
	end
	
	-- initializes random seed
	math.randomseed(os.time())
	-- randomize the sequence
	for i = 1, stim_nbr do
		a = math.random(1, stim_nbr)
		b = math.random(1, stim_nbr)
		swap = sequence[a]
		sequence[a] = sequence[b]
		sequence[b] = swap
	end
end

function process(box)

	local t=0

	-- manages baseline

	box:send_stimulation(1, OVTK_StimulationId_ExperimentStart, t, 0)
	t = t + 5

	box:send_stimulation(1, OVTK_StimulationId_BaselineStart, t, 0)
	box:send_stimulation(1, OVTK_StimulationId_Beep, t, 0)
	t = t + baseline_duration

	box:send_stimulation(1, OVTK_StimulationId_BaselineStop, t, 0)
	box:send_stimulation(1, OVTK_StimulationId_Beep, t, 0)
	t = t + 5

	-- manages trials

	for i = 1, stim_nbr do
	
		-- start of trial
		box:send_stimulation(1, OVTK_GDF_Start_Of_Trial, t, 0)
		
		-- random time before stimulation
		alea = math.random(0, trials_duration*1000-(answer_wait*1000))/1000
		t = t + alea
		
		-- display stimulation
		box:send_stimulation(1, sequence[i], t, 0)
		t = t + target_display
		box:send_stimulation(1, OVTK_StimulationId_SegmentStop, t, 0)
		
		-- wait for the end of trial
		t = t + (trials_duration - alea - target_display)
		
		-- ends trial 
		box:send_stimulation(1, OVTK_GDF_End_Of_Trial, t, 0)
	end

	-- send end for completeness
	box:send_stimulation(1, OVTK_GDF_End_Of_Session, t, 0)
	t = t + 5

	box:send_stimulation(1, OVTK_StimulationId_Train, t, 0)
	t = t + 1
	
	-- used to cause the acquisition scenario to stop
	box:send_stimulation(1, OVTK_StimulationId_ExperimentStop, t, 0)

end
